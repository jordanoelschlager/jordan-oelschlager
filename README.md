Jordan Oelschlager is an experienced Vice President of Operations with a demonstrated history of working in the health care industry at physician practices.

Website: https://jordanoelschlager.com/